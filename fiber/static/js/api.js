window.fiber = {
    run: function(command, cb){
        $.ajax({
            type: 'POST',
            contentType: 'application/json',
            url: "run/",
            data: JSON.stringify({'command': command}),
            dataType: 'json',
            success: function(data){
                if (cb) {
                    cb(data);
                }
            }
        });
    }
}


$(function(){
    // result render function
    function showStatus(result){
        $("#progress").hide();
        if (result.ok){
            $("#log").attr('class', 'text-success');
        } else {
            $("#log").attr('class', 'text-danger');
        }
        if (result.stderr) {
            $("#log").append(result.stderr + '\n');
        }
        if (result.stdout) {
            $("#log").append(result.stdout + '\n');
        }
    };

    $("body").on("click", ".command", function(ev){
        $("#active-cmd-name").text($(this).text());
        $("#progress").show();
        $("#log").empty();
        $("#status").modal();
        fiber.run($(this).data('command'), showStatus);
        ev.preventDefault();
    });
});
