#!/usr/bin/env python
"""Simple web frontend for executinf fabfiles"""

from flask import Flask, jsonify, render_template, request
from fabric import main as fab
import subprocess

app = Flask(__name__)


@app.route('/')
def index():
    fabfile = fab.find_fabfile()
    (docs, commands, defaults) = fab.load_fabfile(fabfile)
    tasks = []
    for cmd, task in commands.items():
        hosts, roles = task.get_hosts_and_effective_roles([], [], [])
        tasks.append({
            'name': task.name,
            'doc': task.wrapped.__doc__.decode('utf-8') if task.wrapped.__doc__ else '',
            'hosts': hosts
        })
    return render_template('index.html', tasks=tasks)


@app.route('/run/', methods=['POST'])
def run_command():
    cmd = request.json.get('command')
    if not cmd:
        return jsonify({
            'stderr': 'No command specified',
            'ok': False
        })
    sh = subprocess.Popen(
        ['fab', "-c", "fabric.rc", cmd], stdout=subprocess.PIPE,
        stderr=subprocess.PIPE, stdin=subprocess.PIPE
    )
    (out, err) = sh.communicate()
    return jsonify({
        'command': cmd,
        'stdout': out,
        'stderr': err,
        'ok': sh.returncode == 0
    })
