# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


setup(
    name='fiber',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        "Flask>=0.10",
        "Fabric>=1.10",
        "Flask-Script>=2.0.5",
    ],
    entry_points={
        'console_scripts': [
            'fiber = fiber.__main__:main'
        ]
    }
)
