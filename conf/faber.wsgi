activate_this = '/opt/faber/devenv/bin/activate_this.py'
execfile(activate_this, dict(__file__=activate_this))

import sys
sys.path.insert('/opt/faber')

from faber import app as application
