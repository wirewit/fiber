#!/usr/bin/env python
from fabric.api import task, run, sudo, hosts, local


@hosts('root@sample host')
@task
def restart_nutcracker():
    """Restart nutcracker"""
    run("service nutcracker restart")


@hosts('root@redis-1.db', 'root@redis-2.db')
@task
def redis_restart():
    """Restart redis at 6379"""
    run("service redis_6379 restart")


@task
def sample_name():
    print("test")
    local("ls -al /etc/")


@task
def uptime():
    local("uptime")
